/*
 * File:   version.h
 * Author: mat1
 *
 * Created on June 1, 2015, 8:07 PM
 */

#ifndef VERSION_H
#define	VERSION_H

#ifdef	__cplusplus
extern "C" {
#endif

const int PARTICLE_v040 = 0x00040000;
const int PARTICLE_v041 = 0x00040100;
const int PARTICLE_v042 = 0x00040200;
const int PARTICLE_v043 = 0x00040300;

const int PARTICLE_VERSION = PARTICLE_v043;


#ifdef	__cplusplus
}
#endif

#endif	/* VERSION_H */

