# Photon Firmware that serves a Soft AP Connection Page

## Warning: The changes here are out of date. Read this section!
This repository builds this [SoftAP Page](https://github.com/mebrunet/softap-setup-page) into `v0.4.3` of the Photon's firmware, which has buggy HTTP softap endpoints. 

For a considerably better user experience, build the `softap-page` branch of the firmware I submitted as a pull request against `develop`. It can be found [here](https://github.com/spark/firmware/pull/582), with full source [here](https://github.com/mebrunet/firmware/tree/softap-page). Alternatively, toy with the code and inject it into your own build of `v0.4.4`.

## Updates

- 2015/08/24 - This SoftAP setup page is now building with v0.4.4 and develop. It works without bugs when run from those versions of the code. With these firmware versions the page can now also be served from external host. i.e. CORS issues have been resolved. I haven't updated this repo yet, but you can find the latest version in this [pull request](https://github.com/spark/firmware/pull/582). 

- 2015/08/21 - The CSS is now passable, and I tightened up a few bugs. Ready for beta-testers...?

- 2015/08/20 (later...) - Fixed several bugs with the process and adds *rudimentary* (laughable) css + responsive design. I would call this hacker/maker ready.

- 2015/08/20 - After banging my head against the wall for waaay too many hours, I *finally* have a **working prototype** (ugly and buggy... but working). Please flash it and give me some feedback! I'll continue to improve the code this week. Note I've only tried this with Chrome Version 44.0.2403.155 (64-bit).

## Description
This project aims to create a version of [Particle's Photon](http://www.particle.io) firmware that does not require an app to establish a WiFi connection.

Instead, when put into Listening Mode, the Photon serves a simple html/javascript page that walks the user through connecting the device to their WiFi. It assumes the device has already been claimed to a Particle account (as would be the case if re-distributing the Photon as the heart of an IoT product).

![Screenshot 2015-08-21 at 15.35.24.png](https://bitbucket.org/repo/ozM94A/images/517157467-Screenshot%202015-08-21%20at%2015.35.24.png)

## Navigating this Repository
This repository started with a single-branch clone of Particle's firmware branch [photon_043](https://github.com/spark/firmware/tree/photon_043). 

The master branch of this repository contains the changes that add a Soft AP connection page, however do so in a *"generic"* or unbranded way. 

Other branches may be unmerged functional tweaks, or brand/product specific changes.

## Instructions for use

### 1- Tool up

First tool up to flash your device's firmware locally by following Particle's [getting started instructions](https://github.com/spark/firmware/blob/develop/docs/gettingstarted.md) for the Photon.

**Helpful Notes:**

- When I wrote this, the link to the "Download and Install Dependencies" in the above getting started guide was broken, so I've posted it [here](https://github.com/spark/firmware/blob/develop/docs/dependencies.md).

- For step 1, the easiest way to install on Ubuntu can be found [here](https://launchpad.net/~terry.guo/+archive/ubuntu/gcc-arm-embedded).

- For step 3, using Ubuntu (both 12.04 & 14.04) I needed to re-install dfu-utils with [this](http://www.the-jedi.co.uk/downloads/arduino/dfu-util_0.8-3.1_amd64.deb) deb package to get the toolchain working.

### 2- Flash your device with *genuine* Particle firmware 
It's probably easiest if you follow their [instructions](https://github.com/spark/firmware/blob/develop/docs/gettingstarted.md#4-flash-it) fully and make sure you can flash the device with code I haven't meddled with first. 

### 3- Flash *this* firmware

a) Clone this repository and checkout branch master. Alternatively, if you're afraid of git, download branch master [here](https://bitbucket.org/mebrunet/photon-firmware/downloads). 

b) Put the device into DFU mode (you should know how to do this if you followed step 2), then.

c) From the project's root directory, on a linux distro:

`cd modules`

`sudo make PLATFORM=photon clean all program-dfu`

*Note - I needed sudo for this to work properly, but there is probably a way to change file permissions and avoid it...*

Windows and Mac users can presumably use Particle's instructions to flash. (i.e. Step 3 is just Step 2 using *this* firmware instead of Particle's)

### 4- Run
*This is the only step a client using a product built on the Photon (and running this firmware) would see.*

Put the Photon into Listening Mode, connect to it's access point (SSID: Photon-XXXX), and using a modern browser visit.

`192.168.0.1/`

The rest should be self explanatory. 

**Note:** One of the strange bugs I'm experiencing is that the Photon exhibits inconsistent behavior when sent the "connect" command. The device may SOS (flash red), reboot, and connect (not terrible), OR do nothing and require a manual reset to connect (annoying). Will keep you posted.

## Helpful Tricks

### Connecting to both the Internet and your Photon's SoftAP

One of the issues in playing around with the Photon in SoftAP mode is that you lose your WiFi connection. I've found two ways to avoid this.

1) Connect your machine via ethernet to you local network, and connect your WiFi to the Photon's app.

2) If you don't have an ethernet port, use an Android Phone's USB tethering instead. i.e. connect your Android phone to the local WiFi, then connect it via usb to your laptop, then in the Hotspot menu select "usb tether" and it simulates an ethernet conection with your machine over usb.

In both cases you now have access to both the Photon and the Internet.

### Postman and disabling CORS

If you want to toy with the HTTP API from your browser, a great tool is [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en).

However, if you want to use jQuery, you'll need to disable [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing), by using this [chrome extension](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en) for example.

## How to edit / customize this firmware for your product.

I've moved the tools needed to develop the hosted site over to this [repository](https://github.com/mebrunet/softap-setup-page). Head there if you'd like to understand what I did and tweak the code yourself.

## Useful links
- [Particle's Soft AP over HTTP documentation](https://github.com/spark/firmware/blob/develop/hal/src/photon/soft-ap.md)